<?php 
	require('conexion.php');
	session_start();
if(isset($_SESSION['ID_PAIS'])){
	if ($_SESSION['ID_PAIS'] == "1"){
		$SQL_UNIVERSIDADES ="SELECT *
							 FROM Universidades
							 WHERE ID_PAIS=1";
		$RESULTADO_UNIVERSIDADES = mysqli_query($link,$SQL_UNIVERSIDADES) or die (mysqli_error($link));
	}
	if ($_SESSION['ID_PAIS'] == "2"){
		$SQL_UNIVERSIDADES ="SELECT *
							 FROM Universidades
							 WHERE ID_PAIS=2";
		$RESULTADO_UNIVERSIDADES = mysqli_query($link,$SQL_UNIVERSIDADES) or die (mysqli_error($link));
	} else {}
}
	$SQL_UNIVERSIDADES_BAR ="SELECT *
						 FROM Universidades
						 LIMIT 5";
	$RESULTADO_UNIVERSIDADES_BAR = mysqli_query($link,$SQL_UNIVERSIDADES_BAR) or die (mysqli_error($link));
	$SQL_UNIVERSIDADES ="SELECT *
						 FROM Universidades";
	$RESULTADO_UNIVERSIDADES = mysqli_query($link,$SQL_UNIVERSIDADES) or die (mysqli_error($link));
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
		<meta name="ms.loc" content="ar">
		<meta charset="UTF-8">
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    	<![endif]-->
		<title>Office Race</title>
		<link rel="shortcut icon" type="image/x-icon" href="//www.microsoft.com/favicon.ico?v2">
		<link rel="stylesheet" type="text/css" href="dist/css/bootstrap-material-design.css">
		<link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="assets/reset.css">
		<link rel="stylesheet" type="text/css" href="assets/font_css.css" />
		<link rel="stylesheet" type="text/css" href="assets/style_type.css">
		<script src="assets/jquery.2.1.3.min.js"></script><!--  JQUERY 2.1.3-->
		<meta property="og:title" content="Office Race empezó. Sumate y hacé que tu universidad gane el Microsoft Imagine Academy."/>
        <meta property="og:site_name" content="Office Race empezó. Sumate y hacé que tu universidad gane el Microsoft Imagine Academy."/>
		<meta property="og:description" content="Compartí con tus compañeros y amigos el ranking. ¡Desafialos a que sigan en carrera para ganar!" />
		<meta property="og:image" content="https://www.quieromioffice.com/images/compartir_ranking.png"/> 
		<meta property="og:url" content="https://www.quieromioffice.com/ofice_race">
	</head>
	<?php if(isset($_SESSION['ID_PAIS'])){?>
	<script>
	$(document).ready(function(){
		$('.registro_nuevo, .intro_login, .premios, .terminos, .ranking').fadeOut(10);
		$('.office_race').delay( 500 ).fadeIn(500);	
		});	
	</script>
	<?php } if(isset($_SESSION['ID_PAIS'])){
		if ($_SESSION['ID_PAIS'] == "1") { ?>
	<body style="background-image: url(images/BackGral_ar.jpg);">
	<?php
	$SQL_UNIVERSIDADES2 ="SELECT *
						 FROM Universidades 
						 WHERE ID_PAIS=1";
	$RESULTADO_UNIVERSIDADES2 = mysqli_query($link,$SQL_UNIVERSIDADES2) or die (mysqli_error($link));
	 } if ($_SESSION['ID_PAIS'] == "2") { ?>
	<body style="background-image: url(images/BackGral_uy.jpg);">
	<?php
	$SQL_UNIVERSIDADES2 ="SELECT *
						 FROM Universidades 
						 WHERE ID_PAIS=2";
	$RESULTADO_UNIVERSIDADES2 = mysqli_query($link,$SQL_UNIVERSIDADES2) or die (mysqli_error($link)); } }?>
	<body style="background-image: url(images/BackGral.jpg);">

		<header>
			<div class="container">
				<div class="col-md-3 logo">
					<img src="images/logo_microsoft.png" alt="">
				</div>
				<div class="col-md-3 pull-right ">
					<?php if(isset($_SESSION['ID_PAIS'])){?>
					<div class="data_login">
					Hola <b style="text-transform: capitalize;"><?php echo $_SESSION['NOMBRE'];?></b>  <br>
					<a href="logout.php">Cerrar Sesión</a></div>
					<a href="javascript:void(0)" onclick="openNav()"><i class="material-icons prefix">view_headline</i></a>
					<div id="mySidenav" class="sidenav">
					<div class="container">
						<a href="javascript:void(0)" class="closebtn " onclick="closeNav()">&times;</a>
						<div class="col-md-6 col-md-push-3 ">
							<a id="office_race" onclick="closeNav()"><img src="images/home_03.png" alt="">¿Qué es OfficeRace?</a>
							<hr>
							<a id="ranking"onclick="closeNav()"><img src="images/home_06.png" alt="">Ranking</a>
							<hr>
							<a  id="premios"onclick="closeNav()"><img src="images/home_08.png" alt="" >Premios</a>
							<hr>
							<a  id="terminos" onclick="closeNav()"><img src="images/home_11.png" alt="">Términos y condiciones</a>
							<hr>
						</div>
						<footer>
							<div class="container">
								<div class="row">
									<div class="col-md-3 social">
										<a href="https://twitter.com/MSFTArgentina" target="_blank">
											<img src="images/twitter.png" alt="">
										</a>
										<a href="https://www.facebook.com/MSFTArgentina/?fref=ts" target="_blank">
											<img src="images/facebook.png" alt="">
										</a>
									</div>
									<div class="col-md-3 pull-right logo_slide">
										<img src="images/logo_microsoft.png" alt="">
									</div>
								</div>
							</div>
						</footer>
					</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</header>
		<section class="intro_login">
			<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-push-3 text-center">
				<img src="images/logo_office_race.png" class="img-responsive center-block">
				</div>
			</div>
				<div class="row">
				<?php if(isset($_GET['error'])){
				$error = $_GET['error'];
					if ($error == "none") {
				  ?> <h2 class="text-center">¡Muchas gracias por registrarte<br><small>Recibirás una confirmación de registro vía e-mail.</small></h2>
				  <div class="text-center col-centered col-md-3">
				  <a class="btn btn-raised btn-danger " name="action" href="index.php">Iniciar Sesión </a>
				  </div>

				  <?php
				
					}
				if ($error == "1") {
				  ?> <h2 class="text-center">Este mail ya se encuentra registrado, por favor ingresa con otro mail</h2>
				  <div class="text-center col-centered col-md-3"><img src="images/loading_animation.gif" alt=""></div>

				  <?php
				  	header("Refresh: 2; url=index.php");
				}
				}
				else { ?>
					<h4 class="text-center">Ya sos parte. ¡Estás a punto de largar!</h4>
					<form class="col-md-6 col-md-push-3" action="verificar_login.php" method="post" id="login">
						<div class="row">
							<div class="col-md-7 col-centered">
								<i class="material-icons prefix">account_circle</i>
								<input placeholder="Ingresá el correo de tu universidad" autocapitalize="off" id="MAIL" name="EMAIL" type="text" class="validate">
							</div>
						</div>
						<div class="row">
							<div class=" col-md-7 col-centered">
								<i class="material-icons prefix">https</i>
								<input placeholder="Contraseña" id="PASSWORD" autocapitalize="off" name="PASSWORD_IN" type="password" class="validate">
							</div>
							<BR>
						</div>
						<div class="row"> 
							<div class="col-xs-6">
								<button class="btn btn-raised btn-danger pull-right" type="submit" name="action">Iniciar Sesión </button>
							</div>
							<div class="col-xs-6">
								<div class="btn btn-raised btn-danger registrarse">Registrarse </div>
							</div>
						</div>
						<div id="mensaje" class=" col-xs-7 col-centered text-center"></div>
					</form>
				<?php } ?>
				</div>
			</div>
		</section>
		<div id="ranking_slide">
			<div class="row">
			<div class="col-md-2 col-xs-2 open_close_rank">
				<img src="images/ranking_sem.png" alt="">
			</div>
				<div class=" univer">
				<h3 class="col-md-10 col-xs-10 col-centered text-center">Top 5 de la semana</h3>
				<ul>
					<div class="number_list">
						<li><small>#</small>1</li>
						<li><small>#</small>2</li>
						<li><small>#</small>3</li>
						<li><small>#</small>4</li>
						<li><small>#</small>5</li>
					</div>
					<div class="list_univer">
					<?php
					$contador12= 1; 
					// $porcentaje = array('100','90','80','76','67','58','50','45', '40', '38','22', '15', '9', '5');
					$porcentaje2 = array('100','10','10','10','10','10');
					while ($uniListado2 = mysqli_fetch_assoc($RESULTADO_UNIVERSIDADES_BAR)) {?>
						<li >
						<h4><?php echo $uniListado2['UNIVERSIDAD']  ?></h4>
						<div class="progress progress-striped">
						  	<div class="progress-bar progress-bar-danger" style="width:<?php echo$porcentaje2[$contador12] ?>%"></div>
						</div> </li>

					<?php  $contador12++; } ?>
					</div>
				</ul>
				<div class="row"> 
							<div class="col-md-10 col-centered text-center">
							<?php if(isset($_SESSION['ID_PAIS'])){?>
							<a id="ranking2" class="btn btn-raised btn-danger">Ver Ranking Completo <i class="material-icons prefix fa fa-long-arrow-right  "></i></a>
							<?php }else{ ?>
							<p>Iniciá sesión para ver el ranking completo</p>
							<?php } ?>
							</div>
				</div>
				</div>
			</div>
		</div>	
		<section class="registro_nuevo">
			<div class="container">
				<div class="row">
				<div class="col-md-8 col-md-push-2 text-center">
			<img src="images/logo_office_race.png" class="img-responsive center-block">
					<h4>Ya sos parte. ¡Estás a punto de largar!</h4>
				</div>
					<div class="row">
						<div class="col-md-7 col-centered text-center">
							<form action="registro_usuario.php" method="post">
								<div class="row">
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-user "></i>
										<input placeholder="Nombre" name="NOMBRE" type="text" class="validate" autocapitalize="off">
										<!-- <label for="email">Email</label> -->
									</div>
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-user "></i>
										<input placeholder="Apellido" name="APELLIDOtype="text" class="validate" autocapitalize="off">
										<!-- <label for="email">Email</label> -->
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
									<i class="material-icons prefix fa fa-envelope "></i>
									<input placeholder="Ingresá el correo de tu universidad" name="MAIL" type="text" class="validate" autocapitalize="off">
									<!-- <label for="email">Email</label> -->	
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-lock "></i>
										<input placeholder="Contraseña" name="PASSWORD" type="password" class="validate" autocapitalize="off">
										<!-- <label for="email">Email</label> -->
									</div>
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-lock "></i>
										<input  placeholder="Repetir contraseña" name="PASSWORD2" type="password" class="validate" autocapitalize="off">
										<!-- <label for="email">Email</label> -->
									</div>
								</div>							
								<div class="row">
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-building "></i>
										<select placeholder="Ingresá el correo de tu universidad" name="ID_UNIVERSIDAD">
											<option value="">Universidad</option>
										<?php while ( $row = mysqli_fetch_assoc($RESULTADO_UNIVERSIDADES)) {
											?>
											<option value="<?php echo $row['ID_UNIVERSIDAD'] ?>"><?php echo $row['UNIVERSIDAD'] ?></option>

										<?php }  ?>
										</select>
										<!-- <label for="email">Email</label> -->
									</div>
									<div class="col-md-6">
										<i class="material-icons prefix fa fa-globe "></i>
										<select placeholder="Ingresá el correo de tu universidad" name="ID_PAIS">
											<option  name="ID_PAIS" value="">País</option>
											<option  name="ID_PAIS" value="1">Argentina</option>
											<option  name="ID_PAIS" value="2">Uruguay</option>

										</select>
										<!-- <label for="email">Email</label> -->
									</div>
								</div>
								<button class="btn btn-raised btn-danger col-centered" type="submit" id="enviar_datos" name="action">Registrarse 
										<i class="material-icons prefix fa fa-long-arrow-right  "></i>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="premios">
			<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-push-3 text-center">
			<img src="images/logo_office_race.png" class="img-responsive center-block">
					<h4>Ya sos parte. ¡Estás a punto de largar!</h4>
				</div>
			</div>
				<div class="row text-center">
				<h4> <img src="images/premios_03.png" alt=""> Todos los estudiantes de las instituciones ganadoras podrán:</h4>
				<p><i class="material-icons prefix fa fa-check "></i> Certificarse para demostrar sus capacidades con las últimas tecnologías.</p>
				
				<p><i class="material-icons prefix fa fa-check "></i> Desarrollar pensamiento crítico con análisis de datos.</p>	
				
				<p><i class="material-icons prefix fa fa-check "></i> Obtener entrenamientos reales</p>
				
				<p><i class="material-icons prefix fa fa-check "></i> Poseer habilidades de vanguardia</p>
				<a href="https://portal.office.com/start?sku=e82ae690-a2d5-4d76-8d30-7c6e01e6022e" target="_blank" class="btn btn-raised btn-danger" ><img src="images/premios_07.png" alt=""> Descargá Office 365 gratis ahora</a>
				</div>
			</div>
		</section>
		<section class="office_race">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-push-2 text-center">
						<h2 class="text-center">¿Qué es Office Race?</h2>
						<p class="text-center">Es una iniciativa de Microsoft que tiene como objetivo que la institución pueda ganar el Imagine Academy y así garantizar el éxito de sus estudiantes y educadores. Fue creado para acortar la brecha entre la educación y las nuevas demandas del mundo laboral. Además, brinda valiosos recursos de desarrollo profesional para los educadores.</p>
						<div class="row">
							<div class="col-md-6 col-centered bajada">
								<div class="col-md-8 col-centered">¡Sumate ahora mismo!<br>
								Viví la experiencia Office Race.</div>
							<a href="https://portal.office.com/start?sku=e82ae690-a2d5-4d76-8d30-7c6e01e6022e" target="_blank"  class="btn btn-raised btn-danger" ><img src="images/premios_07.png" alt=""> Descargá Office 365 gratis ahora</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>		
		<section class="terminos">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-push-2 text-center">
						<h2 class="text-center">Términos y condiciones</h2>
						<p class="text-center">rem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						<p class="text-center">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariaturctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea comtate velit esse cillum dolore eu fugiat nulla pariatur.</p>

						<p class="text-center">ectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

						<div class="row">
							<div class="col-md-6 col-centered bajada">
								<div class="col-md-8 col-centered">¡Sumate ahora mismo!<br>
								Viví la experiencia Office Race.</div>
							<a href="https://portal.office.com/start?sku=e82ae690-a2d5-4d76-8d30-7c6e01e6022e" target="_blank"  class="btn btn-raised btn-danger" ><img src="images/premios_07.png" alt=""> Descargá Office 365 gratis ahora</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="ranking" >
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-push-2 text-center">
					<img src="images/logo_office_race.png" class="img-responsive center-block">
						<h4>Mirá la posición donde está tu institución.</h4>
					</div>
				<div class="col-md-8 col-md-push-2 text-center">
					<div class="row">
						<div class="mostrarRanking">
							<br>
									<?php if(isset($_SESSION['ID_PAIS'])){
										if ($_SESSION['ID_PAIS'] == "1") { ?>
							<div class="col-md-6  text-center cuadro_ranking" style="margin-top: 0px;">
								<div class=" univer">
								<ul>
									<div class="number_list">
										<li><small>#</small>1</li>
										<li><small>#</small>2</li>
										<li><small>#</small>3</li>
										<li><small>#</small>4</li>
										<li><small>#</small>5</li>
									</div>
									<div class="list_univer text-left">
										<?php
										$contador1= 1; 
										// $porcentaje = array('100','90','80','76','67','58','50','45', '40', '38','22', '15', '9', '5');
										$porcentaje = array('100','10','10','10','10','10','10','10', '10', '10','10', '10', '10', '10');
										while ($uniListado = mysqli_fetch_assoc($RESULTADO_UNIVERSIDADES2)) {?>
											<li>
											<h4><?php echo $uniListado['UNIVERSIDAD'] ?></h4>
											<div class="progress progress-striped">
											  	<div class="progress-bar progress-bar-danger" style="width:<?php echo $porcentaje[$contador1] ?>%"></div>
											</div> 
											</li>
										<?php  
							                if ($contador1 == 5 ) {
							                    echo "							
							                    </div>
												</ul>
												</div>		
												</div>						
												<div class='col-md-6  text-center cuadro_ranking_1'>
												<div class=' univer'>
												<ul>
													<div class='number_list'>
														<li><small>#</small>6</li>
														<li><small>#</small>7</li>
														<li><small>#</small>8</li>
														<li><small>#</small>9</li>
														<li><small>#</small>10</li>
													</div>
												<div class='list_univer text-left'>";					                  
							                }					               
							                if ($contador1 == 10 ) {
							                    echo "							
							                    </div>
												</ul>
												</div>		
												</div>						
												<div class='col-md-6 col-md-offset-3 text-center cuadro_ranking2'>
												<div class=' univer'>
												<ul style='height:49px;'>
													<div class='number_list'>
														<li><small>#</small>11</li>
													</div>
												<div class='list_univer text-left'>";					                  
							                }            
							                $contador1++;
							                }
							            ?>
									</div>
									</ul>
								</div>							
							</div>
							<?php } else { ?>
							<div class=" col-md-6 col-md-offset-3 text-center cuadro_ranking">
								<div class=" univer">
								<ul>
									<div class="number_list">
										<li><small>#</small>1</li>
										<li><small>#</small>2</li>

									</div>
									<div class="list_univer text-left">
										<?php
										$contador1= 1; 
										// $porcentaje = array('100','90','80','76','67','58','50','45', '40', '38','22', '15', '9', '5');
										$porcentaje = array('100','10','10');
										while ($uniListado = mysqli_fetch_assoc($RESULTADO_UNIVERSIDADES2)) {?>
											<li>
											<h4><?php echo $uniListado['UNIVERSIDAD'] ?></h4>
											<div class="progress progress-striped">
											  	<div class="progress-bar progress-bar-danger" style="width:<?php echo $porcentaje[$contador1] ?>%"></div>
											</div> 
											</li>
										<?php }?>
									</div>
									</ul>
								</div>							
							</div>
							<?php }
							}?>
						</div>
						<?php if(isset($_SESSION['ID_PAIS'])){
							if ($_SESSION['ID_PAIS'] == "1") { ?>
								<div class="flechasRanking">
								<a class="arribaRanking"><i class="fa fa-arrow-up" aria-hidden="true"></i></a><br>
								<a class="abajoRanking"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>
								</div>
							<?php }
						}?>
					</div>
					<div class="row cta_social">
						<h3 class="text-center">¡No te quedes atrás! Compartí</h3>
					</div>
					<div class="row b_social"> 
						<div class="col-xs-6 ">
								<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.quieromioffice.com%2Foffice_race%2Fpreview_digital"  target="_blank"><div class="btn btn-raised facebook pull-right"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i><span>FACEBOOK</span> </div></a>
						</div>
						<div class="col-xs-6 ">
							<a href="https://twitter.com/intent/tweet?hashtags=OfficeRace&text=Compartí con tus compañeros y amigos el ranking. ¡Desafialos a que sigan en carrera para ganar!&url=https://www.quieromioffice.com/ofice_race/" target="_blank"><div class="btn btn-raised twitter pull-left"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i><span>TWITTER</span> </div></a>
						</div>
					</div>
				</div>
				</div>
			</div>
		</section>	
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-4 pull-right">
					<ul>
						<li><p> 2016 © Microsoft</p></li>
						<li><img src="images/office_logo.png" alt=""></li>
					</ul>
					</div>
				</div>
			</div>
		</footer>
		<?php mysqli_close($link); ?>					
		<script src="assets/bootstrap.js"></script>
		<script src="assets/app.js"></script>
		<script>
		function openNav() {
		    document.getElementById("mySidenav").style.width = "100%";
		}
		function closeNav() {
		    document.getElementById("mySidenav").style.width = "0";
		}
		</script>     
		<!--  INICIO WEDECX & GOOGLE CODE -->
	    <script type="text/javascript">
			var varSegmentation = 0;
			var varClickTracking = 1;
			var varCustomerTracking = 1;
			var varAutoFirePV =1;
			var Route="";
			var Ctrl=""
			document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
	    </script>
	    <noscript><img src="http://c.microsoft.com/trans_pixel.aspx" id="ctl00_MscomBI_ctl00_ImgWEDCS" width="1" height="1" /></noscript>
		<!--  FIN WEDECX & GOOGLE CODE -->
		<!-- INICIO TRACKING CODE GOOGLE ANALITYCS BA DIGITAL -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-xxxxxxx', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- FIN TRACKING CODE GOOGLE ANALITYCS BA DIGITAL -->
    </body>
</html>