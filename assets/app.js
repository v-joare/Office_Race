$(document).ready(function(){
	$('.registrarse').click(function(){
		$('.intro_login').fadeOut(500);
	$('.registro_nuevo').delay( 500 ).fadeIn(500);
	});
	$('.logo').on('click touchend',function(){
		$('.registro_nuevo, .premios, .office_race, .terminos, .ranking').fadeOut(500);
		$('.intro_login').delay( 500 ).fadeIn(500);
	});
	if ($(window).width() < 480) {
	$('#premios').click(function(){
		$('.registro_nuevo, .intro_login, .office_race, .terminos, .ranking').fadeOut(500);
		$('.premios').delay( 500 ).fadeIn(500);
		$('body').css('backgroundPosition',' center 130px');
	});
	$('#office_race').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .terminos, .ranking').fadeOut(500);
		$('.office_race').delay( 500 ).fadeIn(500);
		$('body').css('backgroundPosition',' center 130px');
	});
	$('#terminos').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .ranking').fadeOut(500);
		$('.terminos').delay( 500 ).fadeIn(500);
		$('body').css('backgroundPosition',' center 320px');
	});
	$('#ranking').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .terminos').fadeOut(500);
		$('.ranking').delay( 500 ).fadeIn(500);
		$('body').css('backgroundPosition',' center 130px');
	});
	$('#ranking2').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .terminos').fadeOut(500);
		$('.ranking').delay( 500 ).fadeIn(500);
		$('#ranking_slide').removeClass('up_rank');
		$('body').css('backgroundPosition',' center 130px');
	});
	} else {
	$('#premios').click(function(){
		$('.registro_nuevo, .intro_login, .office_race, .terminos, .ranking').fadeOut(500);
		$('.premios').delay( 500 ).fadeIn(500);
	});
	$('#office_race').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .terminos, .ranking').fadeOut(500);
		$('.office_race').delay( 500 ).fadeIn(500);
	});
	$('#terminos').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .ranking').fadeOut(500);
		$('.terminos').delay( 500 ).fadeIn(500);
	});
	$('#ranking').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .terminos').fadeOut(500);
		$('.ranking').delay( 500 ).fadeIn(500);
	});
	$('#ranking2').click(function(){
		$('.registro_nuevo, .intro_login, .premios, .office_race, .terminos').fadeOut(500);
		$('.ranking').delay( 500 ).fadeIn(500);
		$('#ranking_slide').removeClass('up_rank');
	});		
	}
	$('.open_close_rank').click(function(){
		$('#ranking_slide').toggleClass('up_rank');
	});
	$('#enviar_datos').click(function (){
		function validateEmail(campoEmail) {
			var filtro = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
			if (filtro.test(campoEmail)) {
				return true;
			}
			else {
				return false;
				}
		};
		$(".error").remove();
        if( $('input[name="NOMBRE"]').val() == "" ){
            $('input[name="NOMBRE"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su Nombre</span>").fadeIn('fast'));
            return false;
        }else if( $('input[name="APELLIDO"]').val() == ""){
            $('input[name="APELLIDO"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su Apellido</span>").fadeIn('fast'));
            return false;        
        }else if($('input[name="MAIL"]').val() == ""){
            $('input[name="MAIL"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su Correo</span>").fadeIn('fast'));
            return false;
        }else if(validateEmail($('input[name="MAIL"]').val()) == false){
            $('input[name="MAIL"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese un mail válido</span>").fadeIn('fast'));
            return false;
        }else if($('input[name="PASSWORD"]').val() == ""){
            $('input[name="PASSWORD"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su Contraseña</span>").fadeIn('fast'));
            return false;
        }else if($('input[name="PASSWORD2"]').val() !== $('input[name="PASSWORD"]').val()){
            $('input[name="PASSWORD2"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Sus contrseñas no coinciden</span>").fadeIn('fast'));
            return false;
        }else if( $('select[name="ID_UNIVERSIDAD"]').val() == "" ){
            $('select[name="ID_UNIVERSIDAD"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su universidad</span>").fadeIn('fast'));
            return false;
        }else if( $('select[name="ID_PAIS"]').val() == "" ){
            $('select[name="ID_PAIS"]').focus().after($("<br><span class='center-block error' style='color:red; font-size:11px; float:left;'>*Ingrese su país </span>").fadeIn('fast'));
            return false;
        }
    });
	if ($(window).width() < 768) {
		$('.abajoRanking').click(function(){
			if($('.cuadro_ranking').css('marginTop') === '-510px') 
			  { }else{
			$('.cuadro_ranking').animate({marginTop: '+=-255'});
			$('.cuadro_ranking_1, .cuadro_ranking2').animate({marginTop: '-0'});
		}
		});
		$('.arribaRanking').click(function(){
			if($('.cuadro_ranking').css('marginTop') === '0px') 
			  { }else{
			$('.cuadro_ranking').animate({marginTop: '+=+255'});
			}
		});	
	} else{
	$('.abajoRanking').click(function(){
		$('.cuadro_ranking, .cuadro_ranking_1').animate({marginTop: '-320'});
		$('.cuadro_ranking2').animate({marginTop: '-0'});
	});
	$('.arribaRanking').click(function(){
		$('.cuadro_ranking, .cuadro_ranking_1').animate({marginTop: '0'});
		$('.cuadro_ranking2').animate({marginTop: '0'});
	});
	}
    $('#login').submit(function() {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            beforeSend: function () {
            $("#mensaje").html("Verificando, espere por favor...");
            },
            success: function(response) {
                var result = response;
                console.log(result);
                if(result == "ok") {	
                    // location.assign("index.php");
                }else{
                    $('#mensaje').html(response);
                }
            }
        });   
        return false;
    });


});